FROM python:3.11-slim

ARG YOUR_ENV

ENV YOUR_ENV=production \
  PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  POETRY_VERSION=1.3.1

# System deps:
RUN pip install "poetry==$POETRY_VERSION"

# Copy only requirements to cache them in docker layer
WORKDIR /opt/collector
COPY poetry.lock pyproject.toml /opt/collector/

# Project initialization:
RUN poetry config virtualenvs.create false \
  && poetry install  --no-interaction --no-ansi --no-root

# Creating folders, and files for a project:
COPY . /opt/collector

WORKDIR /opt/collector
#python3 -m flask --debug --app wsgi.py run --host=0.0.0.0 --port=5000

CMD [ "python3", "-m" , "flask", "--app", "wsgi.py", "run", "--host=0.0.0.0", "--port=5000"]