# Collector - a file indexing app

![Project Stage][project-stage-shield]
![Pipeline Status][gitlab-pipeline-status]
![Maintenance][maintenance-shield]
![Latest Release][gitlab-release-svg]
[![MIT License][license-shield]][license-url]
[![Maintaner][maintainer-shield]]([maintainer-url])
![coverage report][gitlab-coverage-report]
[![codecov](https://codecov.io/gl/sandlin/collector/branch/groot/graph/badge.svg?token=QHTQAHVVBD)](https://codecov.io/gl/sandlin/collector)
![Stars][stars-shield]
[![Issues][issues-shield]](issues-url)
<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Coverage Details](https://sandlin.gitlab.io/collector/)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)


<!-- ABOUT THE PROJECT -->
## About The Project

### Use Cases:


### Built With
* [Poetry](https://python-poetry.org/)


<!-- GETTING STARTED -->
## Getting Started

### Prerequisites

### Installation


<!-- USAGE EXAMPLES -->


<!-- CONTRIBUTING -->
## Contributing



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.



<!-- CONTACT -->
## Contact

James Sandlin - [LinkedIn](https://www.linkedin.com/in/jamessandlin/) - jsandlin@gitlab.com

Project Link: [https://gitlab.com/jsandlin/collector](https://gitlab.com/jsandlin/collector)

[Dependabot](https://dependabot-gitlab.gitlab.io/dependabot/guide/getting-started.html)
<!-- Let's define some variables for ease of use. -->
[gitlab-coverage-report]: https://gitlab.com/sandlin/collector/badges/groot/coverage.svg
[gitlab-pipeline-status]: https://gitlab.com/sandlin/collector/badges/groot/pipeline.svg
[gitlab-release-svg]: https://gitlab.com/sandlin/collector/-/badges/release.svg
[project-stage-shield]: https://img.shields.io/badge/project%20stage-development-yellowgreen.svg
[license-shield]: https://img.shields.io/github/license/hassio-addons/repository-beta.svg

[issues]: https://gitlab.com/sandlin/collector/issues
[maintenance-shield]: https://img.shields.io/maintenance/yes/2022.svg
[maintainer-shield]: https://img.shields.io/badge/maintainer-James_Sandlin-blueviolet
[maintainer-url]: https://gitlab.com/jsandlin

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[stars-shield]: https://img.shields.io/gitlab/stars/sandlin/collector.svg?style=flat-square
[issues-shield]: https://img.shields.io/gitlab/issues/open-raw/sandlin/collector.svg?style=flat-square
[issues-url]: https://gitlab.com/jsandlin/repo/issues
[license-shield]: https://img.shields.io/gitlab/license/jsandlin/repo.svg?style=flat-square
[license-url]: https://gitlab.com/jsandlin/repo/blob/master/LICENSE.txt
[product-screenshot]: images/screenshot.png
