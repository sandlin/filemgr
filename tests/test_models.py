import os
from collector.app import create_app
from collector.models import Scan, Directory, File
from collector.db_engine import initialize_db
from unittest.mock import Mock
import socket
from flask import Flask
from loguru import logger
from collector.controllers.seeker import Seeker
from unittest import TestCase
from sqlalchemy import select
from sqlalchemy.engine.row import Row as sqlalchemyRow
from sqlalchemy.orm import selectinload

#from collector import models
import datetime
#
# import logging
# logger = logging.getLogger(__name__)
class TestModels(TestCase):

    def __init__(self, *args, **kwargs):
        #datetime.datetime = Mock()
        self.test_dt = datetime.datetime.now()
        self.basedir = os.path.abspath(os.path.dirname(__file__))
        self.db_file = os.path.join(self.basedir, 'test_models.sqlite')
        self.db_engine, self.db_session  = initialize_db("sqlite:///" + self.db_file)
        #
        #
        # self.test_csv = "A,B,C,D"
        # self.test_list = ["A", "B", "C", "D"]
        super().__init__(*args, **kwargs)
        #self.app = create_app(env="test")
        #self.app = Flask("test")
       # self.app.config["TESTING"] = True
        #self.app.testing = True

        # This creates an in-memory sqlite db
        # See https://martin-thoma.com/sql-connection-strings/
        #self.app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"

        #client = app.test_client()
    #def on_complete(self):

    # Delete temp database on completion.
    def tearDown(self) -> None:
        self.db_session.remove()
        os.remove(self.db_file)
        super().tearDown()

    def test_directory(self):
        datetime = Mock()
        d = Directory(path=self.basedir)
        testtime = datetime.datetime.now.return_value = self.test_dt

        #now = datetime.datetime.now()
        self.assertIsNone(d.id)
        self.assertEqual(d.path, self.basedir)
        self.assertIsNone(d.time_created)

        d.time_created = testtime
        self.db_session.add(d)
        self.db_session.commit()

        #self.assertEqual(d.id, 1)
        self.assertIsInstance(d.id, int)
        self.assertEqual(d.path, self.basedir)
        self.assertEqual(d.time_created, testtime)

    def test_scan(self):
        d = Directory(path=self.basedir)
        #datetime.datetime.now.return_value = self.test_dt
        d.time_created = self.test_dt

        hn = 'localhost'
        s = Scan(hostname=hn, start_dir=d)
        self.assertIsNone(s.id)
        self.assertEqual(s.hostname, hn)
        self.assertIsNone(s.time_created)
        self.assertIsNone(s.time_completed)
        self.assertIsNone(s.success)

        self.db_session.add(s)
        self.db_session.commit()


        self.assertIsInstance(s.id, int)
        self.assertIsInstance(s.time_created, datetime.datetime)
        self.assertIsNone(s.time_completed)
        self.assertIsNone(s.success)
        self.assertIsInstance(s.start_dir, Directory)
        self.assertIsInstance(s.start_dir.id, int)
        self.assertEqual(s.start_dir.path, self.basedir)


    def test_file(self):
        filename = "foobar"
        filesize = "1234"
        timemod = self.test_dt
        timecre = self.test_dt
        d = Directory(path=self.basedir)
        #datetime.datetime.now.return_value = self.test_dt
        d.time_created = timecre

        f = File(name="foobar")
        #self.assertIsNone()


    #
    # @mock.patch("configparser.ConfigParser")
    # @mock.patch.object(gitlab_controller.GitLabController, 'validate_config')
    # def test_host_exists(self, mocker):
    #     with self.app.app_context():
    #         dbs = self.app.db_session()
    #         host = Host(name=socket.gethostname())
    #         # Create host in DB
    #         dbs.add(host)
    #         dbs.commit()
    #         # Verify it's there.
    #         host_prior = dbs.query(Host).first()
    #         # Verify ID is 1
    #         assert(host_prior.id == 1)
    #         # Kick off Scan
    #         testdir = os.path.abspath(".")
    #         Seeker().scan_dir(testdir)


    #
    # def test_scan_dir(self):
    #     self.app.logger.debug("test_scan_dir")
    #
    #     # Expected
    #     # Ignore time_created
    #     expected_results = {
    #         1: {"id": 1, "path": "/Users/jsandlin/workspaces/sandlin/collector/testdata", "time_created": "2023-05-04 18:35:54"},
    #         2: {"id": 2, "path": "/Users/jsandlin/workspaces/sandlin/collector/testdata/w_xmp", "time_created": "2023-05-04 18:35:54"},
    #         3: {"id": 3, "path": "/Users/jsandlin/workspaces/sandlin/collector/testdata/w_and_wo_xmp", "time_created": "2023-05-04 18:35:54"},
    #         4: {"id": 4, "path": "/Users/jsandlin/workspaces/sandlin/collector/testdata/wo_xmp", "time_created": "2023-05-04 18:35:54"},
    #     }
    #
    #
    #     db = self.app.db_session()
    #     with self.app.app_context():
    #         # Execute the scan
    #         testdir = os.path.abspath("../testdata")
    #         Seeker().scan_dir(testdir)
    #         # Ensure root_dir is in DB
    #         # Ensure Scan is in DB
    #         stmt = select(Scan).where(Scan.id == 1)
    #         rs = db.execute(stmt).scalar_one()
    #
    #         self.assertEqual(rs.id, 1)
    #         self.assertIsNotNone(rs.time_completed)
    #         self.assertEqual(rs.start_dir.id, 1)
    #         self.assertEqual(rs.start_dir.path, testdir)
    #
    #         # Now let's ensure the correct directories were placed in the DB.
    #         for row in db.execute(select(Directory)):
    #             self.assertEqual(row.Directory.path, expected_results[row.Directory.id]['path'])
    #             print(row.Directory)
    #
    # #def test_scan_single_dir(self):
    #
