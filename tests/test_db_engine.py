import os
from collector.app import create_app
from collector.db_engine import get_or_create, initialize_db, get_db_connection
from collector.models import Scan, Directory
from unittest import mock
import socket
from flask import Flask
from loguru import logger
from collector.controllers.seeker import Seeker
from unittest import TestCase
from sqlalchemy.orm import selectinload

import sqlalchemy

#from collector import models
import datetime
#
# import logging
# logger = logging.getLogger(__name__)

class TestDbEngine(TestCase):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def test_get_db_connection(self):
        e, s = get_db_connection("sqlite://")
        self.assertIsInstance(e, sqlalchemy.engine.Engine)
        self.assertIsInstance(s, sqlalchemy.orm.scoping.scoped_session)

    def test_initialize_db(self):
        e, s = initialize_db("sqlite://")
        self.assertIsInstance(e, sqlalchemy.engine.Engine)
        self.assertIsInstance(s, sqlalchemy.orm.scoping.scoped_session)
        #i = inspect(e)
        #schemas = i.get_table_names(schema="main")
        #self.assertIn("hosts", schemas)
    #
    # def test_get_or_create(self):
    #     db_engine, db_session = initialize_db("sqlite://")
    #     #h = Host(name="foo")
    #     #db_session.add(h)
    #     #db_session.commit()
    #     h1 = db_session.query(Host).first()
    #     self.assertEqual(h.name, h1.name)
    #     self.assertEqual(h.id, h1.id)
    #     self.assertEqual(h1.id, 1)
    #     h2 = Host(name="bar")
    #     db_session.add(h2)
    #     db_session.commit()
    #     self.assertNotEqual(h.name, h2.name)
    #     self.assertNotEqual(h.id, h2.id)
    #     self.assertEqual(h2.id, 2)
