# Modeled after:
# https://github.com/flaskbb/flaskbb/blob/83ef79d3735ed6bb7333b8eb0acef5f40a8dccbf/flaskbb/cli/main.py

import click
import os
from flask import current_app
from collector import models
from collector.cli.host import cli as seeker_cli
from collector.cli.db import cli as db_cli
from flask.cli import AppGroup, FlaskGroup, with_appcontext
from collector.controllers.seeker import Seeker

from collector.app import create_app
import logging
from pathlib import Path
from pprint import pprint

# @click.group(cls=FlaskGroup, create_app=create_app)

#cli = FlaskGroup(create_app=create_app)

logger = logging.getLogger(__name__)

def make_app(*args, **kwargs):
    #ctx = click.get_current_context(silent=True)
    #ctx.obj = create_app(args, kwargs)
    app = create_app("dev")
    return app

@click.group(#"cli"
             cls=FlaskGroup,
             create_app=make_app#,
             #invoke_without_command=True
             )
#@click.option("--verbosity", "-v", count=True, default=0)
@click.version_option()
@with_appcontext
@click.pass_context
def ccli(ctx):
    # """Command Line Interface / Entrypoint for FileMgr Collector"""
    ctx.obj = make_app()
    print("-*"*20)
    pprint(ctx.obj.__dict__)
    print("-*" * 20)

    if ctx.invoked_subcommand is None:
        click.echo(ctx.get_help())

    #    click.echo("foo")
        #click.echo(ctx.get_help())


@ccli.command(name='scan', short_help="Scan a Directory")
@click.option('-d', "--dir", metavar="<BASE_DIR>", help="The dir to search", required=True)
@click.pass_obj
def scan_dir(app, dir):
    # Set starting directory for search

    print("_*"*20)
    pprint(app.__dict__)
    print("_*" * 20)
    try:
        sd = os.path.dirname(dir)
        s = Seeker()
        s.scan_dir(dir)
    except Exception as e:
        raise e



ccli.add_command(db_cli, "db")
#
# @ccli.command(help="Scan provided path")
# @click.option("--path", "-p", "--dir", "-d", help="Path to start scan. (default: ~/)")
# @click.option("--extension", "-e", help="Extension of files to index (default: jpg)")
# @click.pass_context
# @with_appcontext
# def scan(ctx, path: str = None, extension: str = "jpg"):
#
#     if path is None:
#         path = Path.home()

