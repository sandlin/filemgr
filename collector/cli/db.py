
import click
from loguru import logger
from flask import Flask
from flask import Blueprint
from flask import current_app
from collector import models
from collector.db_engine import reset_db
from flask.cli import AppGroup, run_command, with_appcontext

cli = AppGroup(name="db", short_help="DB Administration")

@cli.command("reset")
@with_appcontext
def reset():
    """ Seed the Database """
    # Register SqlAlchemy
    from collector.models import Directory
    #db = SessionLocal()
    logger.debug("cli/__init__/seed_db()")
    reset_db(current_app.db_engine)
    logger.debug("Tables dropped & recreated")

@cli.command("tables")
@with_appcontext
def tables():
    """ List Tables """
    tl = models.Base.metadata.tables.keys()
    logger.debug("TABLES:")
    for t in tl:
        logger.debug("- {}".format(t))
