#from collector.app import create_app
"""
    collector
    ~~~~~~~
    Collector is a file indexing tool.
    :copyright: (c) 2022 by James Sandlin
    :license: BSD, see LICENSE for more details.
    :ref: https://github.com/flaskbb/flaskbb/blob/83ef79d3735ed6bb7333b8eb0acef5f40a8dccbf/flaskbb/__init__.py#L18
"""
__version__ = "0.0.1"

import logging

logger = logging.getLogger(__name__)