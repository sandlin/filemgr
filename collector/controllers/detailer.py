import os
import datetime
from PIL import Image
from PIL.ExifTags import TAGS
import eyed3
from pprint import pformat
from loguru import logger
from flask import current_app as myapp
from collector import models
from sqlalchemy import ext
from collector.db_engine import get_or_create

class Detailer:

    def __init__(self):
        super().__init__()


    def process_file_via_type(self, a_file):
        if a_file.extension is not None:
            match a_file.extension:
                case 'jpg':
                    #logger.debug(f"{a_file.name} => {a_file.extension}")
                    self.load_image_data(a_file)
                case 'mp3':
                    #logger.debug(f"{a_file.name} => {a_file.extension}")
                    self.load_mp3_info(a_file)
                case _:
                    logger.debug(f"{a_file.name} => ELSE")

    def load_mp3_info(self, a_file):
        db = myapp.db_session()
        eyed3.log.setLevel("ERROR")
        af = eyed3.load(a_file.full_path)
        try:
            #logger.debug(f"gid={af.tag.genre.id} gname={af.tag.genre.name}")
            gen, _ = get_or_create(db, models.Genre, genre_id=af.tag.genre.id, genre_name=af.tag.genre.name)
        except AttributeError:
            gen, _ = get_or_create(db, models.Genre, id=0, genre_id=0, genre_name=None)
        try:
            mp3, _ = get_or_create(db, models.Mp3, file=a_file, genre = gen, artist = af.tag.artist, release_date = str(af.tag.recording_date), album = af.tag.album, album_artist = af.tag.album_artist)
        except AttributeError:
            mp3, _ = get_or_create(db, models.Mp3, file=a_file, genre = gen)
        return mp3

    def load_image_data(self, a_file):
        image = Image.open(a_file.full_path)
        # extract other basic metadata
        info_dict = {
            "Filename": image.filename,
            "Image Size": image.size,
            "Image Height": image.height,
            "Image Width": image.width,
            "Image Format": image.format,
            "Image Mode": image.mode,
            "Image is Animated": getattr(image, "is_animated", False),
            "Frames in Image": getattr(image, "n_frames", 1)
        }
        # extract EXIF data
        exifdata = image.getexif()
        # for label, value in info_dict.items():
        #     logger.debug(f"{label:25}: {value}")
        # extract EXIF data
        exifdata = image.getexif()

        # iterating over all EXIF data fields
        for tag_id in exifdata:
            # get the tag name, instead of human unreadable tag id
            tag = TAGS.get(tag_id, tag_id)
            data = exifdata.get(tag_id)
            # decode bytes
            if isinstance(data, bytes):
                data = data.decode()
            #logger.debug(f"{tag:25}: {data}")
