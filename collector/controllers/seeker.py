import os
import datetime
import logging
from loguru import logger
logger.add(logging.StreamHandler, level="CRITICAL")
from flask import current_app as myapp
from collector import models
import socket
from collector.controllers.detailer import Detailer
from sqlalchemy import ext
from collector.db_engine import get_or_create

class Seeker:

    def __init__(self):
        super().__init__()

    def scan_dir(self, dir_path):

        # Establish DB session
        db = myapp.db_session()

        # Gather host information
        hostname = socket.gethostname()
        #logger.debug("hostname = {}".format(hostname))


        # Ensure Directory is in DB
        scan_dir, _ = get_or_create(db, models.Directory, path=dir_path)
        #logger.debug(scan_dir)

        # # Insert Scan into DB
        scan_x = models.Scan(hostname=hostname, start_dir=scan_dir)
        db.add(scan_x)
        db.commit()
        #logger.debug(scan_x)
        # logger.debug(scan_x)

        # Do the actual scanw
        try:
            #print(scan_dir.path)
            self._walk_path(scan_dir)
            # Scan is done. Mark it complete.
            scan_x.success = True
            db.commit()
        except Exception as e:
            myapp.logger.error(e)
            raise e
            #this_scan.success = False
            #db.commit()

        # # Process the scan



    def _walk_path(self, root_dir):
        #logger.debug(f"root_dir = {root_dir.path}")
        db = myapp.db_session()
        detailer = Detailer()
        for thisdir, subdirs, subfiles in os.walk(root_dir.path):
            # Insert subdir into DB
            td, _ = get_or_create(db, models.Directory, path = thisdir)
            #logger.debug(f"dir => {td}")
            for fn in subfiles:
                fp = os.path.join(thisdir, fn)
                #logger.debug(f"fn => {fn}")
                s = os.path.getsize(fp)
                ct = datetime.datetime.fromtimestamp(os.path.getctime(fp))
                mt = datetime.datetime.fromtimestamp(os.path.getmtime(fp))
                #logger.debug(f"mt = {mt}")
                f, _ = get_or_create(db, models.File, name=fn, size = s, time_created = ct, time_modified = mt, directory = td)
                detailer.process_file_via_type(f)
                #logger.debug(f"file => {f}")
