from flask import Blueprint, jsonify
from collector.models import Directory

directory_bp = Blueprint('dir_bp', __name__)
dir_dao = Directory()

@directory_bp.route("/api/directory", methods = ["GET"])
def get_dirs():
    return jsonify(dir_dao.get_all())
