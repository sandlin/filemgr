from flask import Blueprint, render_template, abort
from jinja2 import TemplateNotFound

default_bp = Blueprint('default', __name__,
                        template_folder='../templates')
#
# @default_bp.route('/', defaults={'page': 'index'})
# @default_bp.route('/<page>')
# def show(page):
#     try:
#         return render_template('pages/%s.html' % page)
#     except TemplateNotFound:
#         abort(404)
# #

@default_bp.route('/')
def index():
    #return "This is an example app"
    return render_template('index.html')