# https://levelup.gitconnected.com/structuring-a-large-production-flask-application-7a0066a65447
# Services/DAO Singleton Design Pattern: A pattern I have found that works very well in a Flask app is the services/dao (database access object) singleton pattern. The summary of this pattern is outlined below:
#
# Business logic is placed in services methods.
# Database access (ORM) is in dao methods which use models.
# Routes are kept light and either use service or dao methods.


# Use SQLAlchemy, not FLASK-SQLAlchemy
# https://towardsdatascience.com/use-flask-and-sqlalchemy-not-flask-sqlalchemy-5a64fafe22a4

import os
from sqlalchemy import Column, Integer,  String, ForeignKey, DateTime, Boolean, Date #UniqueConstraint
from sqlalchemy.ext.hybrid import hybrid_property
# from sqlalchemy.types import Date
from sqlalchemy.orm import relationship, backref, Mapped
from sqlalchemy.sql import func
from collector.db_engine import Base
# from collector.utils import json_serial

from loguru import logger
import json
# https://docs.sqlalchemy.org/en/14/orm/basic_relationships.html#one-to-many
# A one to many relationship places a foreign key on the child table referencing the parent. relationship() is then specified on the parent, as referencing a collection of items represented by the child:

# 2.0 ORM USAGE
# https://docs.sqlalchemy.org/en/14/changelog/migration_20.html#migration-orm-usage
# backref is a shortcut for configuring both parent.children and child.parent relationships at one place only on the parent or the child class (not both).
# back_populates you must put both sides of relationship
class Directory(Base):
    __tablename__ = "directories"
    id = Column(Integer, primary_key=True)
    path = Column(String(512), unique=False, nullable=False)
    time_created = Column(DateTime(timezone=True), server_default=func.now())

    def reprJSON(self):
        try:
            return dict(
                id=self.id,
                #host=self.host.reprJSON(),
                path=self.path,
                time_created=str(self.time_created)
            )
        except:
            return dict()

    def __str__(self):
        return json.dumps(self.reprJSON())

    def __repr__(self):
        return self.__str__()

class Scan(Base):
    __tablename__ = "scans"
    id = Column(Integer, primary_key=True)
    time_created = Column(DateTime(timezone=True), server_default=func.now())
    time_completed = Column(DateTime(timezone=True), onupdate=func.now())
    success = Column(Boolean, unique=False, default=None)
    hostname = Column(String(256), unique=False, nullable=False)
    start_dir_id = Column(Integer, ForeignKey("directories.id"), nullable=False)
    start_dir = relationship("Directory")
    def reprJSON(self):
        try:
            return dict(
                id=self.id,
                time_created=str(self.time_created),
                time_completed=str(self.time_completed),
                success=self.success,
                start_dir=self.start_dir.reprJSON(),
                hostname=str(self.hostname)
            )
        except:
            return dict()

    def __str__(self):
        return json.dumps(self.reprJSON())

    def __repr__(self):
        return self.__str__()


# Directory.sub_directories = relationship("Directory", order_by = Directory.id, back_populates = "directory")

class File(Base):
    __tablename__ = "files"
    id = Column(Integer, primary_key=True)
    name = Column(String(256))
    #extension = Column(String(10))
    size = Column(Integer)
    time_modified = Column(DateTime(timezone=True))
    time_created = Column(DateTime(timezone=True))
    directory_id = Column(Integer, ForeignKey('directories.id'))
    directory = relationship("Directory")

    #files: Mapped["Directory"] = relationship(back_populates="files")

    time_indexed = Column(DateTime(timezone=True), server_default=func.now())

    @hybrid_property
    def extension(self):
        return self.name.split(".")[-1]

    @hybrid_property
    def full_path(self):
        return os.path.join(self.directory.path, self.name)

    def reprJSON(self):
        try:
            return dict(
                id=self.id,
                name=self.name,
                size = self.size,
                time_modified = str(self.time_modified),
                time_created = str(self.time_created),
                extension = self.extension,
                directory=self.directory.reprJSON(),
                time_indexed=str(self.time_indexed)
            )
        except:
            return dict()

    def __str__(self):
        return json.dumps(self.reprJSON())

    def __repr__(self):
        return self.__str__()

class Image(Base):
    __tablename__ = "images"
    id = Column(Integer, primary_key=True)
    file_id = Column(Integer, ForeignKey('files.id'))
    file = relationship("File")

class Song(Base):
    __tablename__ = "songs"
    id = Column(Integer, primary_key=True)
    file_id = Column(Integer, ForeignKey('files.id'))
    file = relationship("File")

    #genre = Column(String(128))
    artist = Column(String(128))
    release_date = Column(String(128))
    album = Column(String(128))
    album_artist = Column(String(128))

    genre_id = Column(Integer, ForeignKey('genres.id'))
    genre = relationship("Genre")

class Genre(Base):
    __tablename__ = "genres"
    id = Column(Integer, primary_key=True)
    genre_id = Column(Integer, unique=True)
    genre_name = Column(String(128), unique=True)