from collector import create_app

'''
This file will be used to run the server locally
wsgy.py replaces this file when deploying.
'''

if __name__ == "__main__":
    app = create_app(env="dev")
    app.run(host="0.0.0.0", port=8000, debug=True)

