

#import logging
import os
from logging.config import dictConfig
# Import flask and template operators
import requests
from flask import Flask, render_template #logging as flog, session, g #, # _app_ctx_stack # render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import scoped_session
from collector.routes.directory import directory_bp
from collector.routes import default_bp
from collector.config import config, loggingConfig
from collector.db_engine import initialize_db
#from collector.cli.db import bp as db_bp
#
# logger = logging.getLogger(__name__)
#logging.basicConfig(filename='record.log', level=logging.DEBUG)
def create_app(env="test"):
    """Create Flask App
    Modeled after
    https://github.com/flaskbb/flaskbb/blob/8args, **kwargs3ef79d3735ed6bb7333b8eb0acef5f40a8dccbf/flaskbb/app.py
    : param env: The environment you are running
    """

    dictConfig(loggingConfig)
    # Create app instance
    if env == "test":
        app = Flask("test")
    else:
        app = Flask(__name__)

    # CORS(app) - https://towardsdatascience.com/use-flask-and-sqlalchemy-not-flask-sqlalchemy-5a64fafe22a4
    # Ensure instance folder exists.
    if not os.path.exists(app.instance_path):
        os.makedirs(app.instance_path)

    #logging.basicConfig(level=app.config['LOG_LEVEL'], format=app.config["LOG_FORMAT"])

    # Load config
    app.config.from_object(config.get(env or 'default'))

    app.logger.setLevel(app.config['LOG_LEVEL'])
    # Configure logger
    # flog.default_handler.setFormatter(app.config['LOG_FORMAT'])
    # flog.default_handler.setLevel(app.config['LOG_LEVEL'])

#    app.logger.setLevel(app.config['LOG_LEVEL'])

    # Establish DB conn
    app.db_engine, app.db_session  = initialize_db(app.config['SQLALCHEMY_DATABASE_URI'])

    # Register blueprints
    app.register_blueprint(default_bp)
    app.register_blueprint(directory_bp, url_prefix='/api')
    #app.register_blueprint(directory_bp)
#    app.register_blueprint(db_bp)

    @app.shell_context_processor
    def shell_context():
        return {'app': app, 'db': app.db_session}

    @app.teardown_appcontext
    def shutdown_session(exception=None):
        app.db_session.remove()
    return app


def create_web_app(env="foo"):
    """Create Flask App
    Modeled after
    https://github.com/flaskbb/flaskbb/blob/8args, **kwargs3ef79d3735ed6bb7333b8eb0acef5f40a8dccbf/flaskbb/app.py
    : param env: The environment you are running
    """
    #
    # dictConfig(loggingConfig)
    # # Create app instance
    if env == "test":
        app = Flask("test")
    elif env == "dev":
        app = Flask("dev")
    else:
        app = Flask(__name__)
    #
    # app.route = "collector"
    # print("instance_path = {}".format(app.instance_path))
    # # CORS(app) - https://towardsdatascience.com/use-flask-and-sqlalchemy-not-flask-sqlalchemy-5a64fafe22a4
    # # Ensure instance folder exists.
    # if not os.path.exists(app.instance_path):
    #     os.makedirs(app.instance_path)
    #
    # #app.logging.basicConfig(level=app.config['LOG_LEVEL'], format=app.config["LOG_FORMAT"])
    #
    # # Load config
    # app.config.from_object(config.get(env or 'default'))
    #
    # app.logger.setLevel(app.config['LOG_LEVEL'])
    # app.debug = app.config['DEBUG']
    # app.testing = app.config['TESTING']
    # # Configure logger
    # # flog.default_handler.setFormatter(app.config['LOG_FORMAT'])
    # # flog.default_handler.setLevel(app.config['LOG_LEVEL'])
    #
    # #    app.logger.setLevel(app.config['LOG_LEVEL'])
    #
    # # Establish DB conn
    # app.db_engine, app.db_session = initialize_db(app.config['SQLALCHEMY_DATABASE_URI'])

    # Register blueprints
    app.register_blueprint(default_bp)
    #app.register_blueprint(directory_bp, url_prefix='/api')

    return app

