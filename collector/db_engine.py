# https://towardsdatascience.com/use-flask-and-sqlalchemy-not-flask-sqlalchemy-5a64fafe22a4

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import exc
from sqlalchemy.orm import sessionmaker, scoped_session


def get_db_connection(db_url):
    db_engine = create_engine(
        db_url, connect_args={"check_same_thread": False}
    )

    # http://www.sqlalchemy.org/docs/06/orm/session.html?highlight=scoped_session#unitofwork-contextual
    # the scoped_session() function is provided which produces a thread-managed registry of Session objects. It is commonly used in web applications so that a single global variable can be used to safely represent transactional sessions with sets of objects, localized to a single thread.
    # This is the connection to be used
    DBSession = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=db_engine))

    return db_engine, DBSession
# DB Objects will extend this.
# Using SQLAlcehmy’s declarative_base() allows you to write just one model per table that app uses. That model is then used in Python outside of the app and in the database.
Base = declarative_base()

def reset_db(db_engine):
    #engine, _ = get_db_connection(db_url)
    import collector.models
    Base.metadata.drop_all(bind=db_engine)
    Base.metadata.create_all(bind=db_engine)

def initialize_db(db_url):
    engine, session = get_db_connection(db_url)
    import collector.models
    Base.metadata.create_all(bind=engine)
    return engine, session

# https://stackoverflow.com/a/2587041
def get_or_create(session,
                      model,
                      create_method='',
                      create_method_kwargs=None,
                      **kwargs):
    try:
        return session.query(model).filter_by(**kwargs).one(), True
    except NoResultFound:
        kwargs.update(create_method_kwargs or {})
        try:
            with session.begin_nested():
                created = getattr(model, create_method, model)(**kwargs)
                session.add(created)
            return created, False
        except  exc.IntegrityError:
            return session.query(model).filter_by(**kwargs).scalar_one(), True
