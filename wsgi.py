# https://github.com/flaskbb/flaskbb/blob/83ef79d3735ed6bb7333b8eb0acef5f40a8dccbf/wsgi.py#L2
import os
from collector.app import create_web_app

#_basepath = os.path.dirname(os.path.abspath(__file__))

# will throw an error if the config doesn't exist
app = create_web_app()
# print("-"*30)
# print(app.url_map)
# print("-"*30)
# print(app.config['DEBUG'])
# print("-"*30)
# print("env = {}".format(app.env))
# print("-"*30)


@app.route("/")
def hello():
    return "Hello, Flask!"
#  Uncomment to use the middleware
# https://flask.palletsprojects.com/en/2.1.x/quickstart/#hooking-in-wsgi-middleware
#flaskbb.wsgi_app = ReverseProxyPathFix(flaskbb.wsgi_app)
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')